FROM lagertonne/cstrike:latest

COPY config_files/* /root/cs16/cstrike/
COPY html/maps/*.bsp /root/cs16/cstrike/maps/
COPY html/maps/*.txt /root/cs16/cstrike/maps/
COPY html/maps/*.res /root/cs16/cstrike/maps/
COPY html/*.wad /root/cs16/cstrike/

ENTRYPOINT ["./hlds_run", "-game cstrike", "-insecure", "-port 27016", "+map cs_assault", "-maxplayers 32"]
